terraform {
  backend "http" {
    address="https://gitlab.com/api/v4/projects/40932798/terraform/state/iacstate"
    lock_address="https://gitlab.com/api/v4/projects/40932798/terraform/state/iacstate/lock"
    unlock_address="https://gitlab.com/api/v4/projects/40932798/terraform/state/iacstate/lock"
    username="shavrovaktivcompany"
    lock_method="POST"
    unlock_method="DELETE"
    retry_wait_min=5
  }
}
